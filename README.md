# shopsense-role-demo
row level user access demo for shopsense.co  [shopsense.pmshould.be](shopsense.pmshould.be).

## database design
roles:

| fields name   |   data type   | key  |
| ------------- |:-------------:| ----:|
| id            | int(11)       | PK   |
| name          | varchar(50)   |      |
| add           | text          |      |
| edit          | text          |      |
| delete        | text          |      |
| view          | text          |      |

users:

| fields name   | data type     | key  |
| ------------- |:-------------:| ----:|
| name          | varchar(200)  | PK   |
| password      | text          |      |
| role_id       | int(11)       | FK   |
| enable        | tinyint(1)    |      |

movies:

| fields name   | data type     | key  |
| ------------- |:-------------:| ----:|
| id            | int(11)       | PK   |
| name          | varchar(200)  |      |
| year_released | int(11)       |      |
| revenue       | doyble        |      |
| director      | varchar(200)  |      |
| producer      | varchar(200)  |      |
| actor         | varchar(200)  |      |
| actoress      | varchar(200)  |      |
| user_id       | int(11)       | FK   |

## how it works?
roles table contains what permission that role has. For example:
1. admin user can add, edit and view all fileds on moview table so entry in roles table is as following:

| id | name | add | edit | delete | view |
| ---|------|-----|------|--------|------|
| 1  | admin| movies.* | movies.* | movies.* | movies.* |

2. staff user can edit some fields and view all fields on movies table so entry in roles table is as following:

| id | name | add | edit | delete | view |
| ---|------|-----|------|--------|------|
| 1  | staff|  | movies.name,movies.year_released,movies.actor,movies.actress |  | movies.* |

3. guest user can see some fields on movies table so entry in roles table is as following:

| id | name | add | edit | delete | view |
| ---|------|-----|------|--------|------|
| 1  | staff|  |  |  | movies.name,movies.year_released,movies.actor,movies.actress |

Note: if filed has not any value that means user can not do that perticular operation, any table_name.* means that user has acess for all fields on that table.

## users and login detail

| user name     | password      | detail  |
| ------------- |:-------------:| ----:|
| superadmin    | p0o9i8u7      | to add user and update roles permission |
| admin         | adminadmin    | admin user |
| staff         | staffstaff    | staff user |

You can always reach me if you have any doubt.
Thanks :)

Chirag (blikenoother -[at]- gmail [dot] com)