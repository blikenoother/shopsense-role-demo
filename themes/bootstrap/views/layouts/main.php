<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/styles.css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl;?>/css/validationEngine.jquery.css" type="text/css">

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>

	<?php Yii::app()->bootstrap->register(); ?>
	
	<script src="<?php echo Yii::app()->theme->baseUrl;?>/js/jquery.validationEngine-en.js" charset="utf-8" type="text/javascript"></script>
	<script src="<?php echo Yii::app()->theme->baseUrl;?>/js/jquery.validationEngine.js" charset="utf-8" type="text/javascript"></script>
</head>

<body>

<?php $this->widget('bootstrap.widgets.TbNavbar',array(
    'items'=>array(
        array(
            'class'=>'bootstrap.widgets.TbMenu',
            'items'=>array(
            	array('label'=>'Roles', 'url'=>array('/roles/index'), 'visible'=>UsersForm::model()->showBtn('roles')),
				array('label'=>'Users', 'url'=>array('/users/index'), 'visible'=>UsersForm::model()->showBtn('users')),
				array('label'=>'Movies', 'url'=>array('/movies/index'), 'visible'=>UsersForm::model()->showBtn('movies')),
                array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
                array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
            ),
        ),
    ),
)); ?>

<div class="container" id="page">

	<?php echo $content; ?>

	<div class="clear"></div>

	<div id="footer">
		
	</div><!-- footer -->

</div><!-- page -->
<script type="text/javascript">
$(document).ready(function(){
    $("form").validationEngine();
});

function checkDigit(field, rules, i, options){
	var all_digit = !isNaN(field.val().toString().replace(/[ ]/g, '') );
	if(!all_digit){
		return 'Only number digit allowed.';
    }
}
</script>
</body>
</html>
