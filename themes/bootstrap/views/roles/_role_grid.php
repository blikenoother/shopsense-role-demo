<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered condensed',
    'dataProvider'=>$gridDataProvider,
    'template'=>"{items} {summary} {pager}",
	'pagerCssClass'=>'pagination profilelv-pager',
	'summaryCssClass'=>'profilelv-summary',
    'columns'=>array(
    	array('name'=>'id', 'header'=>'ID'),
        array('name'=>'name', 'header'=>'Name'),
    	array('name'=>'add', 'header'=>'Add'),
    	array('name'=>'edit', 'header'=>'Edit'),
    	array('name'=>'delete', 'header'=>'Delete'),
    	array('name'=>'view', 'header'=>'View'),
        array(
            'class'=>'bootstrap.widgets.TbButtonColumn',
        	'template'=>'{update}&nbsp;&nbsp;{delete}',
        	'buttons'=>array(
        		'update' => array(
        			'url'=>'Yii::app()->controller->createUrl("roles/detail", array("id"=>$data->id))',
        		),
        		'delete' => array(
        			'url'=>'Yii::app()->controller->createUrl("roles/detail", array("id"=>$data->id,"command"=>"delete"))',
        		),
        	),
            'htmlOptions'=>array('style'=>'width: 50px'),
        ),
    ),
)); ?>