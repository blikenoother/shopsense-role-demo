<?php $this->pageTitle=Yii::app()->name;?>

<?php $this->widget('bootstrap.widgets.TbButton', array(
	    'label'=>'Add New',
	    'type'=>'primary',
	    'size'=>'large',
		'url'=>Yii::app()->controller->createUrl("roles/detail"),
	));?>

<?php $this->renderPartial('_role_grid', array('gridDataProvider'=>$gridDataProvider)); ?>

