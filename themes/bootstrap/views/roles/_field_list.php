<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered condensed',
    'dataProvider'=>$dataProvider,
    'template'=>"{items}",
    'columns'=>array(
    	array('name'=>'table', 'header'=>'Table'),
        array('name'=>'fields', 'header'=>'Fields'),
    ),
)); ?>