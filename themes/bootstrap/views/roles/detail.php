<?php /** @var BootActiveForm $form */
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'UsersForm',
    'htmlOptions'=>array('class'=>'well'),
)); ?>

<?php echo $form->errorSummary($model); ?>

<?php $this->widget('bootstrap.widgets.TbAlert', array(
	'block'=>true,
    'fade'=>true,
    'closeText'=>'&times;',
    'alerts'=>array('success'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'))
)); ?>

<?php $this->widget('bootstrap.widgets.TbAlert', array(
	'block'=>true,
    'fade'=>true,
    'closeText'=>'&times;',
    'alerts'=>array('error'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'))
)); ?>

<?php echo $form->textFieldRow($model, 'name', array('class'=>'span3 validate[required]')); ?>
<?php echo $form->textFieldRow($model, 'add', array('class'=>'span3')); ?>
<?php echo $form->textFieldRow($model, 'edit', array('class'=>'span3')); ?>
<?php echo $form->textFieldRow($model, 'delete', array('class'=>'span3')); ?>
<?php echo $form->textFieldRow($model, 'view', array('class'=>'span3')); ?>

<br>
<?php $btnLabel = isset($model->name) ? 'Update' : 'Add';
$this->widget('bootstrap.widgets.TbButton', array(
	'buttonType'=>'submit',
	'label'=>$btnLabel,
	'type'=>'primary',
	'size'=>'normal',
));?>

<br>

<?php $this->renderPartial('_field_list', array('dataProvider'=>$dataProvider)); ?>

<?php $this->endWidget(); ?>