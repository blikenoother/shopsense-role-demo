<?php /** @var BootActiveForm $form */
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'UsersForm',
    'htmlOptions'=>array('class'=>'well'),
)); ?>

<?php echo $form->errorSummary($model); ?>

<?php $this->widget('bootstrap.widgets.TbAlert', array(
	'block'=>true,
    'fade'=>true,
    'closeText'=>'&times;',
    'alerts'=>array('success'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'))
)); ?>

<?php $this->widget('bootstrap.widgets.TbAlert', array(
	'block'=>true,
    'fade'=>true,
    'closeText'=>'&times;',
    'alerts'=>array('error'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'))
)); ?>

<?php
$nameHtml = array('class'=>'span3 validate[required, minSize[2]]');
if (!$model->name) {
	echo $form->hiddenField($model,'newRecord', array('value'=>'1'));
} else {
	$nameHtml['disabled'] = 'disabled';
}
echo $form->textFieldRow($model, 'name', $nameHtml); ?>
<?php echo $form->textFieldRow($model, 'password', array('class'=>'span3 validate[required, minSize[6]]')); ?>
<label for="UsersForm_role_id" class="required">Role <span class="required">*</span></label>
<?php echo $form->dropDownList($model, 'role_id', $roles, array('class'=>'span3', 'options' => array($model->role_id => array('selected' => true)))); ?>

<br>
<?php $btnLabel = isset($model->name) ? 'Update' : 'Add';
$this->widget('bootstrap.widgets.TbButton', array(
	'buttonType'=>'submit',
	'label'=>$btnLabel,
	'type'=>'primary',
	'size'=>'normal',
));?>

<?php $this->endWidget(); ?>