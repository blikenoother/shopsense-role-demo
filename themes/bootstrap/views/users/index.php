<?php $this->pageTitle=Yii::app()->name;?>

<?php $this->widget('bootstrap.widgets.TbButton', array(
	    'label'=>'Add New',
	    'type'=>'primary',
	    'size'=>'large',
		'url'=>Yii::app()->controller->createUrl("users/detail"),
	));?>

<?php $this->renderPartial('_user_grid', array('gridDataProvider'=>$gridDataProvider)); ?>