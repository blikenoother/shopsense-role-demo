<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered condensed',
    'dataProvider'=>$gridDataProvider,
    'template'=>"{items} {summary} {pager}",
	'pagerCssClass'=>'pagination profilelv-pager',
	'summaryCssClass'=>'profilelv-summary',
    'columns'=>array(
        array('name'=>'name', 'header'=>'Name'),
    	array('name'=>'password', 'header'=>'Password'),
    	array('name'=>'roleName', 'header'=>'Role'),
        array(
            'class'=>'bootstrap.widgets.TbButtonColumn',
        	'template'=>'{update}&nbsp;&nbsp;{delete}',
        	'buttons'=>array(
        		'update' => array(
        			'url'=>'Yii::app()->controller->createUrl("users/detail", array("id"=>$data->id))',
        		),
        		'delete' => array(
        			'url'=>'Yii::app()->controller->createUrl("users/detail", array("id"=>$data->id,"command"=>"delete"))',
        		),
        	),
            'htmlOptions'=>array('style'=>'width: 50px'),
        ),
    ),
)); ?>