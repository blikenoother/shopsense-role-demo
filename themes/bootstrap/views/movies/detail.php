<?php /** @var BootActiveForm $form */
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'MoviesForm',
    'htmlOptions'=>array('class'=>'well'),
)); ?>

<?php echo $form->errorSummary($model); ?>

<?php $this->widget('bootstrap.widgets.TbAlert', array(
	'block'=>true,
    'fade'=>true,
    'closeText'=>'&times;',
    'alerts'=>array('success'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'))
)); ?>

<?php $this->widget('bootstrap.widgets.TbAlert', array(
	'block'=>true,
    'fade'=>true,
    'closeText'=>'&times;',
    'alerts'=>array('error'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'))
)); ?>

<?php echo Permission::model()->checkColumnEdit(MoviesForm::model(), 'name') ? $form->textFieldRow($model, 'name', array('class'=>'span3 validate[required]')) : ''; ?>
<?php echo Permission::model()->checkColumnEdit(MoviesForm::model(), 'year_released') ? $form->textFieldRow($model, 'year_released', array('class'=>'span3 validate[required, funcCall[checkDigit]]')) : ''; ?>
<?php echo Permission::model()->checkColumnEdit(MoviesForm::model(), 'revenue') ? $form->textFieldRow($model, 'revenue', array('class'=>'span3 validate[required, funcCall[checkDigit]]')) : ''; ?>
<?php echo Permission::model()->checkColumnEdit(MoviesForm::model(), 'director') ? $form->textFieldRow($model, 'director', array('class'=>'span3 validate[required]')) : ''; ?>
<?php echo Permission::model()->checkColumnEdit(MoviesForm::model(), 'producer') ? $form->textFieldRow($model, 'producer', array('class'=>'span3 validate[required]')) : ''; ?>
<?php echo Permission::model()->checkColumnEdit(MoviesForm::model(), 'actor') ?  $form->textFieldRow($model, 'actor', array('class'=>'span3 validate[required]')) : ''; ?>
<?php echo Permission::model()->checkColumnEdit(MoviesForm::model(), 'actress') ? $form->textFieldRow($model, 'actress', array('class'=>'span3 validate[required]')) : ''; ?>
<br>
<?php $btnLabel = isset($model->id) ? 'Update' : 'Add';
$this->widget('bootstrap.widgets.TbButton', array(
	'buttonType'=>'submit',
	'label'=>$btnLabel,
	'type'=>'primary',
	'size'=>'normal',
));?>

<?php $this->endWidget(); ?>