<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered condensed',
    'dataProvider'=>$gridDataProvider,
    'template'=>"{items} {summary} {pager}",
	'pagerCssClass'=>'pagination profilelv-pager',
	'summaryCssClass'=>'profilelv-summary',
    'columns'=>array(
        array('name'=>'id', 'header'=>'ID', 'visible'=>true),
        array('name'=>'name', 'header'=>'Name', 'visible'=>Permission::model()->checkColumnView(MoviesForm::model(), 'name')),
        array('name'=>'year_released', 'header'=>'Year Released', 'visible'=>Permission::model()->checkColumnView(MoviesForm::model(), 'year_released')),
        array('name'=>'revenue', 'header'=>'Revenue', 'visible'=>Permission::model()->checkColumnView(MoviesForm::model(), 'revenue')),
    	array('name'=>'director', 'header'=>'Director', 'visible'=>Permission::model()->checkColumnView(MoviesForm::model(), 'director')),
    	array('name'=>'producer', 'header'=>'Producer', 'visible'=>Permission::model()->checkColumnView(MoviesForm::model(), 'producer')),
    	array('name'=>'actor', 'header'=>'Actor', 'visible'=>Permission::model()->checkColumnView(MoviesForm::model(), 'actor')),
    	array('name'=>'actress', 'header'=>'Actress', 'visible'=>Permission::model()->checkColumnView(MoviesForm::model(), 'actress')),
        array(
            'class'=>'bootstrap.widgets.TbButtonColumn',
        	'template'=>'{update}&nbsp;&nbsp;{delete}',
        	'buttons'=>array(
        		'update' => array(
        			'url'=>'Yii::app()->controller->createUrl("movies/detail", array("id"=>$data->id))',
        			'visible'=>'Permission::model()->checkColumnEditBtn(MoviesForm::model())',
        		),
        		'delete' => array(
        			'url'=>'Yii::app()->controller->createUrl("movies/detail", array("id"=>$data->id,"command"=>"delete"))',
        			'visible'=>'Permission::model()->checkColumnDeleteBtn(MoviesForm::model())',
        		),
        	),
            'htmlOptions'=>array('style'=>'width: 50px'),
        ),
    ),
)); ?>