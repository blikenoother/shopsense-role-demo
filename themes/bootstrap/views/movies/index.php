<?php $this->pageTitle=Yii::app()->name;?>

<?php if(Permission::model()->checkColumnAdd(MoviesForm::model())) {
	$this->widget('bootstrap.widgets.TbButton', array(
	    'label'=>'Add New',
	    'type'=>'primary',
	    'size'=>'large',
		'url'=>Yii::app()->controller->createUrl("movies/detail"),
	));
}?>

<?php $this->renderPartial('_movie_grid', array('gridDataProvider'=>$gridDataProvider)); ?>