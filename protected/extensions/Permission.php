<?php

class Permission {
	
	public function checkColumnAdd($model){
		$result = false;
		$role = Yii::app()->session['role'];
		try {
			if (in_array($model->tableName().'.*', $role->add)) {
				return true;
			}
		} catch (Exception $e) {}
		return $result;
	}
	
	public function checkColumnEdit($model, $field){
		$result = false;
		$role = Yii::app()->session['role'];
		try {
			if (in_array($model->tableName().'.*', $role->edit) ||
					in_array($model->tableName().'.'.$field, $role->edit)) {
				return true;
			}
		} catch (Exception $e) {}
		return $result;
	}
	
	public function checkColumnView($model, $field){
		$result = false;
		$role = Yii::app()->session['role'];
		try {
			if (in_array($model->tableName().'.*', $role->view) ||
					in_array($model->tableName().'.'.$field, $role->view)) {
				return true;
			}
		} catch (Exception $e) {}
		return $result;
	}
	
	public function checkColumnEditBtn($model){
		$result = false;
		$role = Yii::app()->session['role'];
		try {
			foreach ($role->edit as $f) {
				if (strpos($f, $model->tableName().'.') !== false) {
					return true;
				}
			}
		} catch (Exception $e) {}
		return $result;
	}
	
	public function checkColumnDeleteBtn($model){
		$result = false;
		$role = Yii::app()->session['role'];
		try {
			if (in_array($model->tableName().'.*', $role->add)) {
				return true;
			}
		} catch (Exception $e) {}
		return $result;
	}
	
	public function getFields($models=array()){
		$data = array();
		$i = 0;
	
		foreach($models as $model){
			$table = $model->tableName();
			
			$fields = $table.'.*';
			foreach($model->attributes as $key=>$val) {
				if ($key!='id'){
					$fields .= ', '.$table.'.'.$key;
				}
			}
			
			$d = array('id'=>$i, 'table'=>$table, 'fields'=>$fields);
			array_push($data, $d);
			
			$i++;
		}
	
		return $data;
	}
	
	public static function model(){
		return new Permission;
	}
	
}