<?php

/**
 * This is the model class for table "movies".
 *
 * The followings are the available columns in table 'movies':
 * @property integer $id
 * @property string $name
 * @property integer $year_released
 * @property double $revenue
 * @property string $director
 * @property string $producer
 * @property string $actor
 * @property string $actress
 * @property string $user_id
 *
 * The followings are the available model relations:
 * @property Users $user
 */
class MoviesForm extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Movies the static model class
	 */
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'movies';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, year_released, revenue, director, producer, actor, actress, user_id', 'required'),
			array('id, year_released', 'numerical', 'integerOnly'=>true),
			array('revenue', 'numerical'),
			array('name, director, producer, actor, actress, user_id', 'length', 'max'=>200),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, year_released, revenue, director, producer, actor, actress, user_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'year_released' => 'Year Released',
			'revenue' => 'Revenue',
			'director' => 'Director',
			'producer' => 'Producer',
			'actor' => 'Actor',
			'actress' => 'Actress',
			'user_id' => 'User',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($pagination=array('pageSize' => 5, 'validateCurrentPage'=>false)){
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('year_released',$this->year_released);
		$criteria->compare('revenue',$this->revenue);
		$criteria->compare('director',$this->director,true);
		$criteria->compare('producer',$this->producer,true);
		$criteria->compare('actor',$this->actor,true);
		$criteria->compare('actress',$this->actress,true);
		$criteria->compare('user_id',$this->user_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>$pagination
		));
	}
	
	public function setAttributes($array, $safe=true){
		foreach ($array as $key=>$value){
			try{
				if (Permission::model()->checkColumnEdit($this->model(), $key)) {
					$this->$key = $value;
				}
			} catch(Exception $ex){
			}
		}
	}
	
}