<?php

/**
 * This is the model class for table "users".
 *
 * The followings are the available columns in table 'users':
 * @property string $name
 * @property string $password
 * @property integer $role_id
 * @property integer $enable
 *
 * The followings are the available model relations:
 * @property Movies[] $movies
 * @property Roles $role
 */
class UsersForm extends CActiveRecord
{
	public $id;
	public $roleName;
	public $newRecord = '';
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Users the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'users';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, password, role_id', 'required'),
			array('role_id, enable', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>200),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('name, password, role_id, enable', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'movies' => array(self::HAS_MANY, 'MoviesForm', 'user_id'),
			'roles' => array(self::BELONGS_TO, 'RolesForm', 'role_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'name' => 'Name',
			'password' => 'Password',
			'role_id' => 'Role',
			'enable' => 'Enable',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($pagination=array('pageSize' => 5, 'validateCurrentPage'=>false)){
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('name',$this->name,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('role_id',$this->role_id);
		$criteria->compare('enable',$this->enable);
		$criteria->with = array('roles');
		$criteria->together = true;
		
		$dataProvider =  new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>$pagination
		));
		
		$data = array();
		foreach($dataProvider->data as $d) {
			$d->id = $d->name;
			$d->roleName = $d->roles->name;
			array_push($data, $d);
		}
		$dataProvider->setData($data);
		
		return $dataProvider;
	}
	
	public function showBtn($btnType){
		if ($btnType == 'roles' &&
				Yii::app()->user->isGuest == false &&
				Yii::app()->user->name == 'superadmin') {
			return true;
		} else if ($btnType == 'users' &&
				Yii::app()->user->isGuest == false &&
				Yii::app()->user->name == 'superadmin') {
			return true;
		} else if ($btnType == 'movies' && Yii::app()->user->name != 'superadmin') {
			return true;
		} else {
			return false;
		}
	}
}