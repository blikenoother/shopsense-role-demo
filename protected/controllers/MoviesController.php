<?php

class MoviesController extends Controller
{
	public function beforeAction($action) {
		if (!isset(Yii::app()->session['role']))
			Yii::app()->session['role'] = RolesForm::model()->getRoleDetail(3);
		
		return true;
	}

	public function actionIndex(){
		$gridDataProvider = MoviesForm::model()->search();
		$this->render('index', array('gridDataProvider'=>$gridDataProvider));
	}

	public function actionError(){
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}
	
	public function actionDetail($id=0){
		$model = new MoviesForm;
		$command = isset($_GET['command']) ? $_GET['command'] : '';
		
		if ($id==0 && Permission::model()->checkColumnAdd(MoviesForm::model())==false) {
			$this->render('error', array('error'=>'Sorry, permission denied !'));
			Yii::app()->end();
		}
		
		if ($id!=0)
			$model = $model->findByPk($id);
		
		if ($command=='delete') {
			if (Permission::model()->checkOperation(MoviesForm::model(), 'delete') && $model->delete($id)) {
				Yii::app()->user->setFlash('success', 'Movie deleted successfully.');
			} else {
				Yii::app()->user->setFlash('error', 'Something went wrong, please try again!');
			}
			
			Yii::app()->end();
		}
			
		
		if(isset($_POST['MoviesForm'])){
			$model->setAttributes($_POST['MoviesForm']);
			
			if (!isset($model->id))
				$model->user_id = Yii::app()->user->name;
			
			$message = isset($model->id) ? 'Movie updated successfully.' : 'New movie added successfully.';
			if($model->save()) {
				Yii::app()->user->setFlash('success', $message);
				$model->unsetAttributes();
			}
		}
		
		$this->render('detail', array('model'=>$model));
	}
	
}