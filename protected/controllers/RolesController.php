<?php

class RolesController extends Controller
{
	public function beforeAction($action) {
		if (Yii::app()->user->name == 'superadmin') {
			return true;
		} else {
			return false;
		}
	}

	public function actionIndex(){
		$gridDataProvider = RolesForm::model()->search();
		$this->render('index', array('gridDataProvider'=>$gridDataProvider));
	}

	public function actionError(){
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}
	
	public function actionDetail($id=0){
		$model = new RolesForm;
		$command = isset($_GET['command']) ? $_GET['command'] : '';
		
		if ($id!=0)
			$model = $model->findByPk($id);
		
		if ($command=='delete') {
			if ($model->delete($id)) {
				Yii::app()->user->setFlash('success', 'Role deleted successfully.');
			} else {
				Yii::app()->user->setFlash('error', 'Something went wrong, please try again!');
			}
			
			Yii::app()->end();
		}
			
		
		if(isset($_POST['RolesForm'])){
			$model->attributes = $_POST['RolesForm'];
			
			$message = isset($model->id) ? 'Role updated successfully.' : 'New role added successfully.';
			if($model->save()) {
				Yii::app()->user->setFlash('success', $message);
				$model->unsetAttributes();
			}
		}
		
		$models = array(MoviesForm::model());
		$fields = Permission::model()->getFields($models);	
		$dataProvider = new CArrayDataProvider($fields);
		$dataProvider->setData($fields);
		
		$this->render('detail', array('model'=>$model, 'dataProvider'=>$dataProvider));
	}
	
}