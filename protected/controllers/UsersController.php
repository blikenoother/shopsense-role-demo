<?php

class UsersController extends Controller
{
	public function beforeAction($action) {
		if (Yii::app()->user->name == 'superadmin') {
			return true;
		} else {
			return false;
		}
	}

	public function actionIndex(){
		$gridDataProvider = UsersForm::model()->search();
		$this->render('index', array('gridDataProvider'=>$gridDataProvider));
	}

	public function actionError(){
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}
	
	public function actionDetail($id=''){
		$model = new UsersForm;
		$command = isset($_GET['command']) ? $_GET['command'] : '';
		
		if ($id!='')
			$model = $model->findByPk($id);
		
		if ($command=='delete') {
			if ($model->delete($id)) {
				Yii::app()->user->setFlash('success', 'User deleted successfully.');
			} else {
				Yii::app()->user->setFlash('error', 'Something went wrong, please try again!');
			}
			
			Yii::app()->end();
		}
			
		
		if(isset($_POST['UsersForm'])){
			$model->attributes = $_POST['UsersForm'];
			
			$message = isset($_POST['UsersForm']['newRecord']) ? 'New user added successfully.' : 'User updated successfully.';
			if(isset($_POST['UsersForm']['newRecord']) &&
					UsersForm::model()->count('name=:name', array(':name'=>$model->name))>0){
				$message = $model->name.' allready present.';
				Yii::app()->user->setFlash('success', $message);
				$model->unsetAttributes();
			} else if($model->save()) {
				Yii::app()->user->setFlash('success', $message);
				$model->unsetAttributes();
			}
		}
		
		$data = RolesForm::model()->findAll();
		$roles = array();
		foreach ($data as $d) {
			$roles[$d->id] = $d->name;
		}
		
		$this->render('detail', array('model'=>$model, 'roles'=>$roles));
	}
	
}